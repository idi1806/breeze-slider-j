/*http://doc.qt.io/qt-5/qml-qtmultimedia-video.html*/

import QtQuick 2.6
import QtMultimedia 5.6

Item {
    MediaPlayer {
        id: mediaplayer
        autoPlay: true
        loops: MediaPlayer.Infinite
        source: config.background
    }

    VideoOutput {
        fillMode: VideoOutput.PreserveAspectCrop
        anchors.fill: parent
        source: mediaplayer
    }
}
