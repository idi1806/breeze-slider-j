/*https://doc.qt.io/qt-5/qml-qtmultimedia-playlist.html*/

import QtQuick 2.6
import QtMultimedia 5.6

Item {
    
    property var i: 0
    property var j: 0

    MediaPlayer {
        id: playlistplayer
        autoPlay: true
        onStatusChanged: {
            shuffleList()
        }
        playlist: Playlist {
            id: playlist
            playbackMode: Playlist.Loop
            property var videoList:
            load("file:///usr/share/sddm/themes/breeze-slider-j/Video-Playlist.m3u")
        }
    }

    VideoOutput {
        fillMode: VideoOutput.PreserveAspectCrop
        anchors.fill: parent
        source: playlistplayer
    }

    function shuffleList() {
        if ( !(i % 2 == 0)) {
            j++
        }
        i++
        if ( j > playlist.itemCount ) { 
            j = 0
            i = 0
        }
        if ( j == 1 ) {
            // more random shuffling
            for (var k = 0; k < Math.ceil(Math.random() * 10) ; k++) {
                playlist.shuffle() 
                
            }
        }
    }
}
