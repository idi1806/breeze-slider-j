import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0
import org.kde.plasma.components 2.0

Item {
    property var clockWidth: Screen.desktopAvailableWidth / 10
    property var clockHeight: Screen.desktopAvailableHeight / 9
    property var desktopWLimit: Screen.desktopAvailableWidth - clockWidth
    property var desktopHLimit: Screen.desktopAvailableHeight - clockHeight

    Rectangle {
        id: floater
        width: childrenRect.width
        height: childrenRect.height
        color: "#80000000"
        
        //ISO 8601: https://en.wikipedia.org/wiki/ISO_8601
        ColumnLayout {
            Label {
                text: Qt.formatTime(timeSource.data["Local"]["DateTime"], " hh:mm ")
                font.pointSize: 48
                Layout.alignment: Qt.AlignHCenter
                color: "white"
            }
            Label {
                text: Qt.formatDate(timeSource.data["Local"]["DateTime"], " yyyy-MM-dd ")
                font.pointSize: 24
                Layout.alignment: Qt.AlignHCenter
                color: "white"
            }
            DataSource {
                id: timeSource
                engine: "time"
                connectedSources: ["Local"]
                interval: 1000
            }
        }

        ParallelAnimation {
            id: animateFloater
            NumberAnimation {
                id: animateX
                target: floater
                properties: "x"
                duration: 5000
            }
            NumberAnimation {
                id: animateY
                target: floater
                properties: "y";
                duration: 1000
            }
            onRunningChanged: {
                animateX.to = Math.floor(Math.random() * desktopWLimit )
                animateY.to = Math.floor(Math.random() * desktopHLimit )
                animateFloater.start();
            }
        }
        Component.onCompleted: {
            animateX.to = Math.floor(Math.random() * desktopWLimit )
            animateY.to = Math.floor(Math.random() * desktopHLimit )
            animateFloater.start();
        }
    }
}
