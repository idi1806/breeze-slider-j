This sddm theme is based on the KDE Breeze theme - plasma-workspace-5.9.4


This sddm theme is using a semitransparent (color: "#F2000000") rectangle. The sddm texts are on the rectangle and moving out the sight when not needed.

The rectangle is moving:
- up when up arrow is pressed 
- down when the down arrow is pressed.
- mouse movement will also trigger the down movement.
- after 3 minute inactivity the rectangle is sliding out of sight.

Using:

plasma-workspace-5.9.4/lookandfeel/contents/lockscreen/LockScreenUi.qml
plasma-workspace-5.9.4/sddm-theme/

More of the  lock screen part - lock screen readability: https://forum.kde.org/viewtopic.php?f=289&t=131783&start=45#p372930


Installation:

Plasma 5.9 or later

Use "Get New Theme / Install From file" from the KDE SDDM control module (kcm-sddm).


Older than Plasma 5.9

Unpack and copy the 'breeze-slider-j' directory to the /usr/share/sddm/themes/. From the cli:

$ sudo cp -R breeze-slider-j /usr/share/sddm/themes/


Testing: 

https://github.com/sddm/sddm/wiki/Theming
'You can test your themes using sddm-greeter. Note that in this mode, actions like shutdown, suspend or login will have no effect.

sddm-greeter --theme /path/to/you/theme'


Errors:

The testing should show if you have all the bit and pieces installed.



Settings:


Solid colour

Set the type and the color at /usr/share/sddm/themes/breeze-slider-j/theme.conf.user

Example: https://forum.kde.org/viewtopic.php?f=289&t=137502#p370239

[General]
type=color
color=darkgreen


Image background

Same as the standard Breeze sddm theme 5.9

Copy picture file (Imagefile.jpg) to the /usr/share/sddm/themes/breeze-slider-j/ and set the theme.conf.user:

[General]
background=Imagefile.jpg
type=image

The KDE sddm module (plasma 5.9) will write correct theme.conf.user for the Breeze slider sddm theme.


Video background

Copy video file (VideoFile.mp4) to the /usr/share/sddm/themes/breeze-slider-j/ and set the theme.conf.user:

[General]
background=VideoFile.mp4
type=video


With the kwriteconfig5:

sudo kwriteconfig5 --file "/usr/share/sddm/themes/breeze-slider-j/theme.conf.user --group General --key background "VideoFile.mp4"
sudo kwriteconfig5 --file /usr/share/sddm/themes/breeze-slider-j/theme.conf.user --group General --key type video


Video playlist background

Copy video playlist (Video-Playlist.m3u) to the /usr/share/sddm/themes/breeze-slider-j/ and set the theme.conf.user:

[General]
background=Video-Playlist.m3u
type=video

sudo kwriteconfig5 --file "/usr/share/sddm/themes/breeze-slider-j/theme.conf.user --group General --key background "Video-Playlist.m3u"
sudo kwriteconfig5 --file /usr/share/sddm/themes/breeze-slider-j/theme.conf.user --group General --key type playlist

More of the video playlist - the 'A KDE service menu to wrap videos to the playlist' part: https://forum.kde.org/viewtopic.php?f=289&t=131783&start=15#p365974


Image slideshow

Link (or copy) image files to the /usr/share/sddm/themes/breeze-slider-j/SlideFiles/

Example command:
sudo ln -s /path/to/images/*.* /usr/share/sddm/themes/breeze-slider-j/SlideFiles/

and set the theme.conf.user:

[General]
type=slideshow


With the kwriteconfig5:
sudo kwriteconfig5 --file /usr/share/sddm/themes/breeze-slider-j/theme.conf.user --group General --key type slideshow


Changes:

Removing:
Clock.qml

Edit:
metadata.desktop
Main.qml
Background.qml

Added:
Floaterclock.qml
SlideShow.qml
VideoPlayer.qml
VideoPlayList.qml



KDE Forum: https://forum.kde.org/viewtopic.php?f=289&t=137502
