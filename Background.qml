/*
 *   Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2

FocusScope {
    id: sceneBackground

    property var sceneBackgroundType
    property alias sceneBackgroundColor: sceneColorBackground.color
    property alias sceneBackgroundImage: sceneImageBackground.source

    Rectangle {
        id: sceneColorBackground
        anchors.fill: parent
    }

    Image {
        id: sceneImageBackground
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        smooth: true;
    }
    
    Loader { 
        id: sceneAlternateBackground
        anchors.fill: parent
    }
    
    Floaterclock {}
    
    states: [
        State {
            name: "slideShowBackground"
            when: sceneBackgroundType == "slideshow"
            PropertyChanges {
                target: sceneColorBackground
                visible: false
            }
            PropertyChanges {
                target: sceneImageBackground
                visible: false
            }
            PropertyChanges {
                target: sceneAlternateBackground
                source: "SlideShow.qml"
            }
        },
        State {
            name: "playListBackground"
            when: sceneBackgroundType == "playlist"
            PropertyChanges {
                target: sceneColorBackground
                visible: false
            }
            PropertyChanges {
                target: sceneImageBackground
                visible: false
            }
            PropertyChanges {
                target: sceneAlternateBackground
                source: "VideoPlayList.qml"
            }
        },
        State {
            name: "videoBackground"
            when: sceneBackgroundType == "video"
            PropertyChanges {
                target: sceneColorBackground
                visible: false
            }
            PropertyChanges {
                target: sceneImageBackground
                visible: false
            }
            PropertyChanges {
                target: sceneAlternateBackground
                source: "VideoPlayer.qml"
            }
        },
        State {
            name: "imageBackground"
            when: sceneBackgroundType == "image"
            PropertyChanges {
                target: sceneColorBackground
                visible: false
            }
            PropertyChanges {
                target: sceneImageBackground
                visible: true
            }
        },
        State {
            name: "colorBackground"
            when: sceneBackgroundType == "color"
            PropertyChanges {
                target: sceneColorBackground
                visible: true
            }
            PropertyChanges {
                target: sceneImageBackground
                visible: false
            }
        }
    ]
}
