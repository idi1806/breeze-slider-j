# breeze-slider-j

Breeze SDDM Theme with sliding ui (QT5.x only - doesn't work with QT6)


### Install
Move the folder to 

    /usr/share/sddm/themes

edit file

    /etc/sddm.conf

search for

    [Theme]
    Current=theme-name

and change to 

    [Theme]
    Current=breeze_slider-j

### History
Original Homepage: https://store.kde.org/p/1174674/
Original Author: David Edmundson
Original Copyright: (c) 2014, David Edmundson
Tampered by Rog131, J.S.

### Old README.txt

This sddm theme is based on the KDE Breeze theme - plasma-workspace-5.9.4

This sddm theme is using a semitransparent (color: "#F2000000") rectangle. The sddm texts are on the rectangle and moving out the sight when not needed.

The rectangle is moving:
- up when up arrow is pressed 
- down when the down arrow is pressed.
- mouse movement will also trigger the down movement.
- after 3 minute inactivity the rectangle is sliding out of sight.

Using:


plasma-workspace-5.9.4/lookandfeel/contents/lockscreen/LockScreenUi.qml

plasma-workspace-5.9.4/sddm-theme/

More of the  lock screen part - lock screen readability: https://forum.kde.org/viewtopic.php?f=289&t=131783&start=45#p372930

KDE Forum: https://forum.kde.org/viewtopic.php?f=289&t=13750
